#ifndef LIGHTPAINTER_H
#define LIGHTPAINTER_H

#include <QWidget>
#include <QPainter>
#include "trafficlightcontroler.h"

namespace tls {

class Lightpainter : public QWidget {
private:
    Q_OBJECT
    TrafficlightControler _control;
    bool _pushed;

    void drawLight(QPainter *painter);
    void cycle();
public:
    explicit Lightpainter(TrafficlightControler &control,QWidget *parent = nullptr);
    virtual ~Lightpainter();

    TrafficlightControler* getTrafficlightControler();
    void cycleDone();
protected:
    void paintEvent(QPaintEvent *event);
    void mousePressEvent(QMouseEvent *event);

signals:

};

}

#endif // LIGHTPAINTER_H
