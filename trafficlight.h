#ifndef TRAFFICLIGHT_H
#define TRAFFICLIGHT_H

#include "light.h"

namespace tls {

class Trafficlight {
private:
    Light _stateCar, _statePedestrian;

    Light* changeState(const Light &light) const;
public:
    explicit Trafficlight();
    explicit Trafficlight(Light light_car, Light light_pedestrain);
    explicit Trafficlight(const Trafficlight &src);
    virtual ~Trafficlight();

    void switchState();

    Light* getTrafficlightStateForCar();
    Light* getTrafficlightStateForPedestrain();
};

}

#endif // TRAFFICLIGHT_H
