#include "lightpainter.h"
#include "switch.h"

#include <QThread>
#include <QMouseEvent>

namespace tls {

/**
 * @brief Constructs a Widget to draw the trafficlights
 * @param control Trafficlightcontroler instance
 * @param parent
 */
Lightpainter::Lightpainter(TrafficlightControler &control,QWidget *parent)
    : QWidget{parent}, _control(control), _pushed(false) {}

Lightpainter::~Lightpainter(){}

/**
 * @brief Draws the trafficlights with QPainter
 * @param painter QPainter to paint inside this Widget
 */
void Lightpainter::drawLight(QPainter *painter){
    QList<Trafficlight> t = _control.getTrafficlights();

    /** Startingpoint **/
    unsigned int x = 128;
    unsigned int y = 192;
    for(auto n = 0; n < t.length(); n++){

        /** 2 trafficlights **/
        if(n == 1){
            x = 384;
            y = 64;

            painter->fillRect(48,160,480,32,QColor(192,192,192));
        }

        /** 3 trafficlights **/
        if(n == 2){
            x = 320;
            y = 256;

            painter->fillRect(48,160,480,32,QColor(192,192,192));
            painter->fillRect(272,160,32,256,QColor(192,192,192));
        }

        /** 4 trafficlights **/
        if(n == 3){
            x = 192;
            y = 32;

            painter->fillRect(48,160,480,32,QColor(192,192,192));
            painter->fillRect(272,0,32,256,QColor(192,192,192));
        }

        /** Get colors which should be drawn **/
        Light *stateCar = t[n].getTrafficlightStateForCar();
        Light *statePen = t[n].getTrafficlightStateForPedestrain();
        switch(*stateCar){
        case Light::RED:
            painter->setPen(Qt::red);
            painter->setBrush(Qt::red);
            painter->fillRect(x,y,32,32,QColor(0,0,0));
            painter->drawEllipse(x+1,y+1,28,28);
            painter->fillRect(x,y+32,32,32,QColor(0,0,0));
            painter->fillRect(x,y+64,32,32,QColor(0,0,0));
            break;
        case Light::RED_YELLOW:
            painter->setPen(Qt::red);
            painter->setBrush(Qt::red);
            painter->fillRect(x,y,32,32,QColor(0,0,0));
            painter->drawEllipse(x+1,y+1,28,28);
            painter->setPen(Qt::yellow);
            painter->setBrush(Qt::yellow);
            painter->fillRect(x,y+32,32,32,QColor(0,0,0));
            painter->drawEllipse(x+1,y+32,28,28);
            painter->fillRect(x,y+64,32,32,QColor(0,0,0));
            break;
        case Light::YELLOW:
            painter->setPen(Qt::yellow);
            painter->setBrush(Qt::yellow);
            painter->fillRect(x,y+32,32,32,QColor(0,0,0));
            painter->drawEllipse(x+1,y+32,28,28);
            painter->fillRect(x,y,32,32,QColor(0,0,0));
            painter->fillRect(x,y+64,32,32,QColor(0,0,0));
            break;
        case Light::GREEN:
            painter->setPen(Qt::green);
            painter->setBrush(Qt::green);
            painter->fillRect(x,y+64,32,32,QColor(0,0,0));
            painter->drawEllipse(x+1,y+64,28,28);
            painter->fillRect(x,y,32,32,QColor(0,0,0));
            painter->fillRect(x,y+32,32,32,QColor(0,0,0));
            break;
        default:
            break;
        }

        switch(*statePen){
        case Light::RED:
            painter->setPen(Qt::red);
            painter->setBrush(Qt::red);
            painter->fillRect(x+32,y,32,32,QColor(0,0,0));
            painter->drawEllipse(x+33,y+1,28,28);
            painter->fillRect(x+32,y+32,32,32,QColor(0,0,0));
            painter->fillRect(x+32,y+64,32,32,QColor(128,128,128));
            painter->setPen(Qt::black);
            painter->drawText(x+34,y+72,32,32,0,"PUSH");
            break;
        case Light::GREEN:
            painter->setPen(Qt::green);
            painter->setBrush(Qt::green);
            painter->fillRect(x+32,y+32,32,32,QColor(0,0,0));
            painter->drawEllipse(x+33,y+33,28,28);
            painter->fillRect(x+32,y,32,32,QColor(0,0,0));
            painter->fillRect(x+32,y+64,32,32,QColor(128,128,128));
            painter->setPen(Qt::black);
            painter->drawText(x+34,y+72,32,32,0,"PUSH");
            break;
        default:
            break;
        }
    }
    delete painter;
}

/**
 * @brief Creates a Thread to run a complete trafficlight cycle
 */
void Lightpainter::cycle(){
    /** deactivate the possibility to create another cycle inside a cycle **/
    _pushed = true;
    QThread *thread = new QThread();
    Switcher *switcher = new Switcher(this);
    switcher->moveToThread(thread);
    connect(thread, SIGNAL(started()), switcher, SLOT(process()));
    thread->start();
}

void Lightpainter::paintEvent(QPaintEvent *event){
    QPainter *painter = new QPainter(this);
    drawLight(painter);
}

/**
 * @brief Examines the moordinates of the mouseclick. Only specified hitboxes shall start
 * a Trafficlightcycle
 * @param event
 */
void Lightpainter::mousePressEvent(QMouseEvent *event){
    unsigned int posx = event->pos().rx();
    unsigned int posy = event->pos().ry();

    /** Trafficlight 1 **/
    if(posx >= 162 && posx <= 194 && posy >= 256 && posy <= 288){
        if(!_pushed){
            cycle();
        }
    /** Trafficlight 2 **/
    } else if(posx >= 420 && posx <= 452 && posy >= 128 && posy <= 160){
        if(!_pushed){
            cycle();
        }
    /** Trafficlight 3 **/
    } else if(posx >= 352 && posx <= 384 && posy >= 320 && posy <= 352){
        if(!_pushed){
            cycle();
        }
    /** Trafficlight 4 **/
    } else if(posx >= 224 && posx <= 256 && posy >= 98 && posy <= 130){
        if(!_pushed){
            cycle();
        }
    }
}

/**
 * @brief returns the Trafficlightcontroler instance
 * @return controlerinstance
 */
TrafficlightControler* Lightpainter::getTrafficlightControler() {
    return &_control;
}

/**
 * @brief Calls from the Thread. Allows a new cycle
 */
void Lightpainter::cycleDone(){
    _pushed = false;
}

}
