#include "trafficlight.h"

namespace tls {

/**
 * @brief Constructs a default trafficlight with the colors red for cars and pedestrains
 */
Trafficlight::Trafficlight() : _stateCar(Light::RED), _statePedestrian(Light::RED) { }

/**
 * @brief Constructs a trafficlight with choosen colors
 * @param light_car trafficlightcolor for cars
 * @param light_pendestrain trafficlightcolor for pedestrains
 */
Trafficlight::Trafficlight(Light light_car, Light light_pendestrain) : _stateCar(light_car), _statePedestrian(light_pendestrain) { }

/**
 * @brief Copyconstructor
 * @param src
 */
Trafficlight::Trafficlight(const Trafficlight &src) : _stateCar(src._stateCar), _statePedestrian(src._statePedestrian) {}

Trafficlight::~Trafficlight(){}

/**
 * @brief switches the lightcolors of the trafficlights for cars and pedestrains according to the actual color
 */
void Trafficlight::switchState() {
    Light *states = changeState(_stateCar);
    _stateCar = states[0];
    _statePedestrian = states[1];
    delete[] states;
}

/**
 * @brief changes the lightcolors and returns the new colors
 * @param light the actual light color for cars
 * @return new lightcolor
 */
Light* Trafficlight::changeState(const Light &light) const {
    Light* states = new Light[2];
    switch(light){
    case Light::RED:
        states[0] = Light::RED_YELLOW;
        states[1] = Light::RED;
        return states;
    case Light::RED_YELLOW:
        states[0] = Light::GREEN;
        states[1] = Light::RED;
        return states;
    case Light::GREEN:
        states[0] = Light::YELLOW;
        states[1] = Light::RED;
        return states;
    case Light::YELLOW:
        states[0] = Light::RED;
        states[1] = Light::GREEN;
        return states;
    default:
        states[0] = Light::RED;
        states[0] = Light::RED;
        return states;
    }
    delete[] states;
}

/**
 * @brief returns lightcolor for cars
 * @return lightcolor for cars
 */
Light* Trafficlight::getTrafficlightStateForCar(){
    return &_stateCar;
}

/**
 * @brief returns lightcolor for pedestrains
 * @return lightcolor for pedestrains
 */
Light* Trafficlight::getTrafficlightStateForPedestrain(){
    return &_statePedestrian;
}

}
