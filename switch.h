#ifndef SWITCH_H
#define SWITCH_H

#include "lightpainter.h"
#include <QObject>

namespace tls {

class Switcher : public QObject {
private:
    Lightpainter *_l;
    Q_OBJECT
    void finished();
public:
    explicit Switcher(Lightpainter *l,QObject *parent = nullptr);
    virtual ~Switcher();
public slots:
    void process();
signals:
};

}

#endif // SWITCH_H
