#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QWidget>
#include "trafficlightcontroler.h"

namespace tls {

class MainWindow : public QWidget {
private:
    Q_OBJECT
    TrafficlightControler _control;

    void initLayout();
public:
    MainWindow(TrafficlightControler &tlc, QWidget *parent = nullptr);
    ~MainWindow();
};

}
#endif // MAINWINDOW_H
