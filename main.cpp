#include "mainwindow.h"
#include "light.h"
#include "trafficlight.h"
#include "trafficlightcontroler.h"

#include <QApplication>

using namespace tls;

int main(int argc, char *argv[]){
    QList<Trafficlight> trafficlightlist = QList<Trafficlight>();
    if(argc == 1){
        qDebug() << "Use a numeric parameter for the amount of trafficlights. 2,3 or 4.";
        return 0;
    } else if (argc == 2){
        QString input = argv[1];
        try {
            int amount = input.toInt();
            if(amount == 2){
                trafficlightlist.append(Trafficlight(Light::RED, Light::GREEN));
                trafficlightlist.append(Trafficlight(Light::GREEN,Light::RED));
            } else if(amount == 3){
                trafficlightlist.append(Trafficlight(Light::RED, Light::GREEN));
                trafficlightlist.append(Trafficlight(Light::RED, Light::GREEN));
                trafficlightlist.append(Trafficlight(Light::GREEN,Light::RED));
            } else if(amount == 4){
                trafficlightlist.append(Trafficlight(Light::RED, Light::GREEN));
                trafficlightlist.append(Trafficlight(Light::RED, Light::GREEN));
                trafficlightlist.append(Trafficlight(Light::GREEN,Light::RED));
                trafficlightlist.append(Trafficlight(Light::GREEN,Light::RED));
            } else {
                qDebug() << "Only amount between 2 and 4 allowed.";
                return 0;
            }
            TrafficlightControler control = TrafficlightControler(trafficlightlist);
            QApplication a(argc, argv);
            MainWindow w(control);
            return a.exec();
        }  catch (const QString &e) {
            qDebug() << "Please input a number 2,3 or 4 for the amount of trafficlights.";
            return 0;
        }
    }
    return 0;
}
