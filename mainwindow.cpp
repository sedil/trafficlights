#include "mainwindow.h"
#include "lightpainter.h"
#include <QBoxLayout>

namespace tls {

/**
 * @brief The mainwindow in which the trafficlights will be drawed
 * @param tlc Trafficlightcontroler instance
 * @param parent
 */
MainWindow::MainWindow(TrafficlightControler &tlc,QWidget *parent)
    : _control(tlc) {
    initLayout();
}

MainWindow::~MainWindow(){}

/**
 * @brief initializes the mainwindow and creates the paintingdevice
 */
void MainWindow::initLayout(){
    Lightpainter *lp = new Lightpainter(_control);
    QBoxLayout *layout = new QBoxLayout(QBoxLayout::TopToBottom);
    layout->addWidget(lp);
    setLayout(layout);
    setFixedSize(720,420);
    show();
}

}
