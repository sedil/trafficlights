#include "switch.h"
#include <QThread>

namespace tls {

/**
 * @brief This class helps to redraw the new lightcolors inside a new Thread
 * @param l Lightpainter instance
 * @param parent
 */
Switcher::Switcher(Lightpainter *l,QObject *parent)
    : QObject{parent}, _l(l) { }

Switcher::~Switcher() { }

/**
 * @brief A complete trafficlight cycle
 */
void Switcher::process(){
    QThread::currentThread()->sleep(5);
    _l->getTrafficlightControler()->controlLightState();
    _l->update();
    QThread::currentThread()->sleep(2);
    _l->getTrafficlightControler()->controlLightState();
    _l->update();
    QThread::currentThread()->sleep(8);
    _l->getTrafficlightControler()->controlLightState();
    _l->update();
    QThread::currentThread()->sleep(2);
    _l->getTrafficlightControler()->controlLightState();
    _l->update();
    finished();
}

/**
 * @brief allows to make another trafficlight cycle
 */
void Switcher::finished(){
    _l->cycleDone();
}

}
