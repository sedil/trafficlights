#ifndef TRAFFICLIGHTCONTROLER_H
#define TRAFFICLIGHTCONTROLER_H

#include <QList>
#include "trafficlight.h"

namespace tls {

class TrafficlightControler {
private:
    QList<Trafficlight> _trafficlightlist;
public:
    explicit TrafficlightControler();
    explicit TrafficlightControler(QList<Trafficlight> &tl);
    explicit TrafficlightControler(const TrafficlightControler &src);
    virtual ~TrafficlightControler();

    void controlLightState();
    QList<Trafficlight> getTrafficlights();
};

}

#endif // TRAFFICLIGHTCONTROLER_H
