#ifndef LIGHT_H
#define LIGHT_H

namespace tls {

/**
 * @brief Trafficlight conditions
 */
enum Light {
    RED,
    RED_YELLOW,
    YELLOW,
    GREEN
};
}

#endif // LIGHT_H
