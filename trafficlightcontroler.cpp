#include "trafficlightcontroler.h"

namespace tls {

/**
 * @brief Defaultconstructor to control all created trafficlights
 */
TrafficlightControler::TrafficlightControler(){
    _trafficlightlist = QList<Trafficlight>();
}

/**
 * @brief Constructor to control all created trafficlights
 * @param tl
 */
TrafficlightControler::TrafficlightControler(QList<Trafficlight> &tl) : _trafficlightlist(tl){ }

/**
 * @brief Copyconstructor
 * @param src
 */
TrafficlightControler::TrafficlightControler(const TrafficlightControler &src) {
    _trafficlightlist = src._trafficlightlist;
}

TrafficlightControler::~TrafficlightControler(){
}

/**
 * @brief Call this function to switch all lightcolors for the trafficlights
 */
void TrafficlightControler::controlLightState(){
    for(Trafficlight &t : _trafficlightlist){
        t.switchState();
    }
}

/**
 * @brief get the list of trafficlights
 * @return list of trafficlights
 */
QList<Trafficlight> TrafficlightControler::getTrafficlights() {
    return _trafficlightlist;
}

}
